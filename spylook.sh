#!/bin/bash
#License GPL-3

MODPROBECONF=/etc/modprobe.d/spydroid.conf
VIDEO_NR=$(cat $MODPROBECONF | grep -o video_nr=[0-9]* | cut -d "=" -f 2)
VIDEO=/dev/video${VIDEO_NR}
echo "Source $VIDEO"
IFACE=$(cat /proc/net/wireless | tail -n1 | cut -d " " -f 2 | tr -d :)
test=$(iwconfig $IFACE 2> /dev/null)
if [ $? -ne 0 ]
then
	IFACE="eth0"
fi
echo "Interface: $IFACE"
IP=$(ifconfig $IFACE | grep -o inet\ [0-9]*[.][0-9]*[.][0-9]*|cut -d " " -f2)
echo "Lan ip: $IP"
PORT=8086
DESTIP=$(nmap -p $PORT -sT  ${IP}.0/24 | awk '{if ($0~"Nmap scan") {ip=$NF} else if ($0~"open") {print ip}}' | tr -d "()")
if [ -z $DESTIP ]
then
	echo "No spydroid stream available"
	sleep 5
	exit 1
fi
echo "Stream ip: ${DESTIP}:${PORT}"
ffmpeg   -re -i  rtsp://${DESTIP}:${PORT} -f v4l2 -vcodec rawvideo -pix_fmt yuv420p $VIDEO
