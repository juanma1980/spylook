# SpyLook

Simple client for [spydroid](https://github.com/fyhertz/spydroid-ipcamera), available from F-Droid [here](https://f-droid.org/en/packages/net.majorkernelpanic.spydroid/)
Spylook will try to autodetect if there's any spydroid accessible from wireless and if one can be reached then redirects the stream to a virtual camera created with v4l2-loopback.

# Installation

There's no installation at all. Simply configure v4l2-loopback to load on boot (examples at modprobe.d and modules-load.d folders)

# Usage

- spylook.sh is an example script. Searches the lan (wifi or eth0) and when finds a spydroid stream tries to connect to it and stablish the bridge between the stream and the virtual camera device

- spylook.py is a QT application that takes full control of a spydroid stream. Scans the network and lists all the spydroid streams available. It can choose a camera from phone, play sounds and start/stop the flash light.

Spylook also supports as many virtual cams as desired. The options must be present at spydroid.conf. Example:

`options v4l2loopback video_nr=9,10,11 devices=3 exclusive_caps=1 card_label="SpyDroidCam1","SpyDroidCam2","SpyDroidCam3"`


Loads thre devices with respective numbers 9, 10 and 11 and respective labels SpyDroidCam1, SpyDroidCam2 and so on.


# Other

In distributions with systemd the module v4l2-loopback could be loaded at startup as:

-  Copy file sysdroid.conf to /etc/modprobe.d. In this file there're the options for loopback module.
-  Copy file sysdroid_module.conf to /etc/modules-load.d. This file tells the system to load the loopback module at startup.


-------------------------------------------


# spylook

Cliente sencillo para [spydroid](https://github.com/fyhertz/spydroid-ipcamera), descargable desde F-Droid [aquí](https://f-droid.org/en/packages/net.majorkernelpanic.spydroid/)
Spylook intenta autodetectar si hay alguna emisión ed spydroid accesible por wifi y si encuentra una entonces la redirige a una cámara virtual creada mediante v4l2-loopback.

# Instalación

No hay instalación como tal. Configura v4l2-loopback para que se cargue al arrancar (ver los ejemplos en los directorios modprobe.d y modules-load.d)

# Uso

- spylook.sh es un script a modo de ejemplo. Busca en la red (wifi o eth0) un stream de spydroid y cuando encuentra uno se intenta conectar y establer la conexión entre el stream y la cámara virtual.

- spylook.py es una aplicación QT que permite controlar la retransisión desde spydroid. Permite seleccionar un dispositivo de red y muestra los spydroid que están retransmitiendo en ella pudiendo conectara cualquiera de ellos. Una vez conectado es posible selecciónar la cámara, encender o apagar el flash, reproducir los sonidos que incluye spydroid o hacer vibrar el teléfono.

Además también es posible trabajar con varias cámaras virtuales. Las opciones deben indicarse en el fichero spydroid.conf. Ejemplo:

`options v4l2loopback video_nr=9,10,11 devices=3 exclusive_caps=1 card_label="SpyDroidCam1","SpyDroidCam2","SpyDroidCam3"`

Crea tres cámaras virtuales respectivamente numeradas como 9,10 y 11 y los nombres SpyDroidCam1, SpyDroidCam2, etc...

# Otros

En distribuciones con systemd l módulo de v4l2-loopback se puede cargar así:

- Copiar el fichero sysdroid.conf a /etc/modprobe.d. En este fichero se encuentran las opciones para el modulo de loopback. 
- Copiar el fichero sysdroid_module.conf a /etc/modules-load.d. En este fichero se indica que el modulo de loopback se cargue en el arranque.

