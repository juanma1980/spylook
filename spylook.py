#!/usr/bin/env python3
import subprocess
import os
import time
import json
import random
import requests
import socket
socket.setdefaulttimeout(0.1)
import array
import fcntl
import struct
from PySide2.QtWidgets import QApplication, QLabel, QPushButton,QGridLayout,QWidget,QComboBox
from PySide2 import QtGui
from PySide2.QtCore import Qt,QThread,Signal,QSignalMapper
import multiprocessing as mp
QString=type("")

MODPROBECONF="/etc/modprobe.d/spydroid.conf"
PORT="8086"

i18njson=os.path.join(os.path.dirname(__file__),"i18n.json")
i18nTable={}
if os.path.isfile(i18njson):
	with open (i18njson,'r') as f:
		i18nTable=json.load(f)

class i18nClass():
	def __init__(self):
		lang=os.getenv('LANG')
		self.lang=lang.split("_")[0]
	
	def get(self,string,rollback=""):
		translation=i18nTable.get(self.lang,i18nTable.get("es",{})).get(string,"")
		if translation=="":
			if rollback!="":
				translation=rollback
			else:
				translation=string
		return(translation)
#class i18n

class th_scanLan(QThread):
	endscan=Signal(list)
	def __init__(self,lanip='',parent=None):
		QThread.__init__(self, parent)
		self.lanip=lanip
	#def __init__

	def run(self):
		streamer=scanLan(self.lanip)
		self.endscan.emit(streamer)	
	#def run
#class th_scanLan

class th_stream(QThread):
	def __init__(self,parent=None):
		QThread.__init__(self, parent)
		self.streamer=''
		self.device=''
		self.process=''
		self.flash="off"
		self.camera="back"
	#def __init__
	
	def setStreamer(self,streamer):
		self.streamer=streamer
	#def setStreamer

	def setDevice(self,device):
		self.device=device
	#def setDevice

	def toggleFlash(self):
		if self.flash=="off":
			self.flash="on"
		else:
			self.flash="off"
	#def enableFlash

	def setCamera(self,camera):
		self.camera="back"
		if camera=="front":
			self.camera=camera
			self.flash="off"
	#def setCamera

	def run(self):
		self.process=mp.Process(target=connectStream,args=(self.streamer,self.device,self.flash,self.camera))
		self.process.start()
	#def run

	def stop(self):
		if not isinstance(self.process,str):
			subprocess.run(["kill","-9",str(self.process.pid+1)])
			subprocess.run(["kill","-9",str(self.process.pid)])
			self.process.terminate()
			self.process.join()
	#def stop
#class th_stream

def getVirtualCams():
	video_nr=-1
	if os.path.isfile(MODPROBECONF):
		with open(MODPROBECONF,'r') as f:
			fcontent=f.readlines()
		for line in fcontent:
			if "video_nr=" in line:
				linesplit=line.split(" ")
				for option in linesplit:
					if option.startswith("video_nr"):
						video_nr=option.split("=")[-1]
				break
	devices=[]
	for devNumber in str(video_nr).split(","):
		if int(devNumber)>=0:
			devices.append("/dev/video{}".format(devNumber))
	return devices
#def getVirtualCams

def getAllIfaces():
	#Code from https://code.activestate.com/recipes/439093-get-names-of-all-up-network-interfaces-linux-only/
	""" Returns a dictionary of name:ip key value pairs. """
	# Max possible bytes for interface result.  Will truncate if more than 4096 characters to describe interfaces.
	MAX_BYTES = 4096
	# We're going to make a blank byte array to operate on.  This is our fill char.
	FILL_CHAR = b'\0'
	# Command defined in ioctl.h for the system operation for get iface list
	# Defined at https://code.woboq.org/qt5/include/bits/ioctls.h.html under
	# /* Socket configuration controls. */ section.
	SIOCGIFCONF = 0x8912
	# Make a dgram socket to use as our file descriptor that we'll operate on.
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	# Make a byte array with our fill character.
	names = array.array('B', MAX_BYTES * FILL_CHAR)
	# Get the address of our names byte array for use in our struct.
	names_address, names_length = names.buffer_info()
	# Create a mutable byte buffer to store the data in
	mutable_byte_buffer = struct.pack('iL', MAX_BYTES, names_address)
	# mutate our mutable_byte_buffer with the results of get_iface_list.
	# NOTE: mutated_byte_buffer is just a reference to mutable_byte_buffer - for the sake of clarity we've defined them as
	# separate variables, however they are the same address space - that's how fcntl.ioctl() works since the mutate_flag=True
	# by default.
	mutated_byte_buffer = fcntl.ioctl(sock.fileno(), SIOCGIFCONF, mutable_byte_buffer)
	# Get our max_bytes of our mutated byte buffer that points to the names variable address space.
	max_bytes_out, names_address_out = struct.unpack('iL', mutated_byte_buffer)
	# Convert names to a bytes array - keep in mind we've mutated the names array, so now our bytes out should represent
	# the bytes results of the get iface list ioctl command.
	namestr = names.tobytes()
	namestr[:max_bytes_out]
	bytes_out = namestr[:max_bytes_out]
	# Each entry is 40 bytes long.  The first 16 bytes are the name string.
	# the 20-24th bytes are IP address octet strings in byte form - one for each byte.
	# Don't know what 17-19 are, or bytes 25:40.
	ip_dict = {}
	for i in range(0, max_bytes_out, 40):
		name = namestr[ i: i+16 ].split(FILL_CHAR, 1)[0]
		name = name.decode('utf-8')
		if name=='lo' or name.startswith('virbr'):
			continue
		ip_bytes   = namestr[i+20:i+24]
		full_addr = []
		for netaddr in ip_bytes:
			if isinstance(netaddr, int):
				full_addr.append(str(netaddr))
			elif isinstance(netaddr, str):
				full_addr.append(str(ord(netaddr)))
		ip_dict[name] = '.'.join(full_addr)
	return ip_dict
#def getAllIfaces

def getLanIP(iface):
	lanip="127.0.0.0/24"
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	ip=socket.inet_ntoa(fcntl.ioctl(
		s.fileno(),
		0x8915,  # SIOCGIFADDR
		struct.pack('256s', bytes(iface[:15],'utf8'))
	)[20:24])
	lanIp=".".join(ip.split(".")[0:3])
	return lanIp
#def getLanIP

def scanLan(lanIp):
	streamer=[]
	for a in range(1,255):
		sock=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		ip="{}.{}".format(lanIp,a)
		testip=(ip, int(PORT))
		result=sock.connect_ex(testip)
		if result==0:
			streamer.append(ip)
			sock.close()
	return(streamer)
#def scanLan

def connectStream(stream,device,flash="on",camera="back"):
	cmd=["ffmpeg","-re","-i","rtsp://{0}:{1}?flash={2}&camera={3}".format(stream,PORT,flash,camera),"-f","v4l2","-vcodec","rawvideo","-pix_fmt","yuv420p",device]
	subprocess.run(cmd,stdout=subprocess.DEVNULL,stderr=subprocess.DEVNULL)
#def connectStream

def makeBuzz():
	params={"action":"buzz"}
	requests.post("http://{}:8080/request.json".format(cmbCams.currentText()),data=str(params))
#def makeBuzz

def playSound(sound):
	params={"action":"play","name":sound}
	requests.post("http://{}:8080/request.json".format(cmbCams.currentText()),data=str(params))
#def playSound

def toggleFlash():
	device=cmbDevices.currentText()
	if device in activeStreams.keys():
		stopCam()
		time.sleep(0.5)
		stream=activeStreams[device].get("stream")
		flash=activeStreams[device].get("flash","")
		if flash=="off":
			activeStreams[device]["flash"]="on"
		else:
			activeStreams[device]["flash"]="off"
		stream.toggleFlash()
		startCam()
#def toggleFlash

def switchCamBack():
	device=cmbDevices.currentText()
	if device in activeStreams.keys():
		stream=activeStreams[device].get("stream")
		activeStreams[device]["camera"]="back"
		stopCam()
		time.sleep(2)
		stream.setCamera("back")
		startCam()
#def switchCamBack

def switchCamFront():
	device=cmbDevices.currentText()
	if device in activeStreams.keys():
		stream=activeStreams[device].get("stream")
		activeStreams[device]["camera"]="front"
		stopCam()
		time.sleep(2)
		stream.setCamera("front")
		startCam()
#def switchCamFront

def stopCam():
	controls.setEnabled(False)
	sounds.setEnabled(False)
	device=cmbDevices.currentText()
	if device in activeStreams.keys():
		stream=activeStreams.get(device)
		stream.get("stream").stop()
		activeStreams[device]["enabled"]==0
		btnStop.setVisible(False)
		btnStart.setVisible(True)
		btnSearch.setEnabled(True)
		cmbCams.setEnabled(True)
		cmbIfaces.setEnabled(True)
#def stopCam


def enableCam(*args):
	if (str(args[0])!=""):
		controls.setEnabled(True)
		sounds.setEnabled(True)
		streamer=str(args[0])
		btnStop.setVisible(True)
		btnStart.setVisible(False)
		btnSearch.setEnabled(False)
		device=cmbDevices.currentText()
		if device in activeStreams.keys():
			stream=activeStreams[device].get("stream")
			activeStreams[device]["enabled"]==1
		else:
			ip=cmbCams.currentText()
			stream=th_stream()
			activeStreams[device]={"streamer":streamer,"stream":stream,"ip":ip,"flash":"off","camera":"back","enabled":1}
			stream.setDevice(device)
			stream.setStreamer(streamer)
		stream.run()
		setStatus()
#def enableCam

def searchCam():
	cursor=QtGui.QCursor(Qt.WaitCursor)
	mw.setCursor(cursor)
	iface=cmbIfaces.currentText()
	lanip=getLanIP(iface)
	scanLan=th_scanLan(lanip=lanip)
	scanLan.endscan.connect(populateCam)
	scanLan.run()
#def searchCam

def startCam():
	txt=cmbCams.currentText()
	if txt!='':
		enableCam(txt)
	return()
#def startCam

def populateCam(*args):
	cams=args[0]
	cmbCams.clear()
	if len(cams)>0:
		for cam in cams:
			cmbCams.addItem(cam)
		btnStop.setVisible(False)
		btnStart.setVisible(True)
		btnStart.setEnabled(True)
	cursor=QtGui.QCursor(Qt.PointingHandCursor)
	mw.setCursor(cursor)
#def populateCam

def populateIfaces():
	cmbIfaces.clear()
	for iface in ifaces.keys():
		cmbIfaces.addItem(iface)
#def populateIfaces():

def populateDevices():
	cmbDevices.clear()
	if len(devices)>0:
		for device in devices:
			cmbDevices.addItem(device)
	else:
		cmbDevices.addItem(i18n.get("NOT_DETECTED","No detectada"))
#def populateDevices():

def setStatus(*args):
	device=cmbDevices.currentText()
	if activeStreams.get(device,{}).get("enabled")==1:
		btnStop.setVisible(True)
		btnStart.setVisible(False)
		btnSearch.setEnabled(False)
		cmbCams.setCurrentText(activeStreams[device].get("ip"))
		cmbCams.setEnabled(False)
		cmbIfaces.setEnabled(False)
		controls.setEnabled(True)
		sounds.setEnabled(True)
	else:
		btnStop.setVisible(False)
		btnStart.setVisible(True)
		btnSearch.setEnabled(True)
		cmbCams.setEnabled(True)
		cmbIfaces.setEnabled(True)
		controls.setEnabled(False)
		sounds.setEnabled(False)
	btnFlash.setChecked(False)
	btnFront.setChecked(False)
	btnRear.setChecked(True)
	if activeStreams.get(device,{}).get("flash")=="on":
		btnFlash.setChecked(True)
	if activeStreams.get(device,{}).get("camera")=="front":
		btnFront.setChecked(True)
		btnRear.setChecked(False)

def renderGui():
	lay=QGridLayout()
	lay.setHorizontalSpacing(6)
	lay.setMargin(6)
	mw.setLayout(lay)
	btnStart.setText(i18n.get("ENABLE","Activar"))
	btnSearch.setText(i18n.get("SEARCH","Buscar"))
	btnStop.setText(i18n.get("STOP","Detener"))
	btnStop.setVisible(False)
	btnStart.setEnabled(False)
	lay.addWidget(btnSearch,3,0,1,1)
	lay.addWidget(btnStart,3,1,1,1)
	lay.addWidget(btnStop,3,1,1,1)
	btnSearch.clicked.connect(searchCam)
	btnStart.clicked.connect(startCam)
	btnStop.clicked.connect(stopCam)
	renderCamControls()
	lay.addWidget(camControls,0,0,1,2,Qt.AlignCenter|Qt.AlignCenter)
	renderControls()
	lay.addWidget(controls,0,2,1,1,Qt.AlignCenter|Qt.AlignCenter)
	renderSounds()
	lay.addWidget(sounds,0,3,1,1,Qt.AlignCenter|Qt.AlignCenter)
	if cmbDevices.currentText()==i18n.get("NOT_DETECTED","No detectada"):
		mw.setEnabled(False)
	cmbDevices.currentTextChanged.connect(setStatus)
	return(mw)
#def renderGui

def renderCamControls():
	vbox=QGridLayout()
	camControls.setLayout(vbox)
	lbl=QLabel(i18n.get("VIRTCAM","Cámara virtual"))
	vbox.addWidget(lbl,0,0,1,1)
	vbox.addWidget(cmbDevices,0,1,1,1)
	lbl=QLabel(i18n.get("DEVICE","Dispositivo"))
	vbox.addWidget(lbl,1,0,1,1)
	vbox.addWidget(cmbIfaces,1,1,1,1)
	lbl=QLabel(i18n.get("CAMIP","Webcam ip"))
	vbox.addWidget(lbl,2,0,1,1)
	vbox.addWidget(cmbCams,2,1,1,1)
#def renderCamControls

def renderControls():
	vbox=QGridLayout()
	controls.setLayout(vbox)
	btnBuzz=QPushButton(i18n.get("BUZZ","Vibrar"))
	btnBuzz.clicked.connect(makeBuzz)
	vbox.addWidget(btnBuzz,0,0,1,1)
	btnFlash.setCheckable(True)
	btnFlash.clicked.connect(toggleFlash)
	vbox.addWidget(btnFlash,1,0,1,1)
	btnFront.setCheckable(True)
	btnFront.setAutoExclusive(True)
	vbox.addWidget(btnFront,2,0,1,1)
	btnRear.setCheckable(True)
	btnRear.setChecked(True)
	btnRear.setAutoExclusive(True)
	vbox.addWidget(btnRear,3,0,1,1)
	btnRear.clicked.connect(switchCamBack)
	btnFront.clicked.connect(switchCamFront)
	controls.setEnabled(False)
#def renderControls

def renderSounds():
	sigmap_run=QSignalMapper(mw)
	sigmap_run.mapped[QString].connect(playSound)
	vbox=QGridLayout()
	sounds.setLayout(vbox)
	soundList=["animals_cat","animals_dog","funny_fart","funny_fart2","funny_snoring","scary_breath","scary_evil_laughter","scary_horror","scary_insane_laughter","scary_long_growl","scary_thunder","war_explosion","war_gunshot"]
	x=0
	y=0
	for sound in soundList:
		btn=QPushButton(i18n.get(sound,sound))
		sigmap_run.setMapping(btn,sound)
		btn.clicked.connect(sigmap_run.map)
		vbox.addWidget(btn,x,y,1,1)
		y+=1
		if y>=4:
			y=0
			x+=1
	sounds.setEnabled(False)
#def renderControls

def quitApp():
	for device,stream in activeStreams.items():
		stream.get("stream").stop()
	app.quit()

activeStreams={}
i18n=i18nClass()
rsrc=os.path.join(os.path.dirname(__file__),"spylook.png")
app=QApplication(["spyLook"])
app.setWindowIcon(QtGui.QIcon(rsrc))
app.lastWindowClosed.connect(quitApp)
mw=QWidget()
camControls=QWidget()
controls=QWidget()
sounds=QWidget()
btnStart=QPushButton()
btnSearch=QPushButton()
btnStop=QPushButton()
cmbDevices=QComboBox()
cmbDevices.setSizeAdjustPolicy(cmbDevices.AdjustToContents)
devices=getVirtualCams()
if len(devices)==0:
	print("No camera found")
populateDevices()
cmbCams=QComboBox()
cmbCams.setSizeAdjustPolicy(cmbCams.AdjustToContents)
cmbIfaces=QComboBox()
cmbIfaces.setSizeAdjustPolicy(cmbIfaces.AdjustToContents)
ifaces=getAllIfaces()
populateIfaces()
btnFlash=QPushButton(i18n.get("FLASH","Flash"))
btnFront=QPushButton(i18n.get("FRONT","Frontal"))
btnRear=QPushButton(i18n.get("REAR","Trasera"))
mw=renderGui()
mw.show()
app.exec_()

